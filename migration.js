require('dotenv').config();

var mysql = require('mysql');
var migration = require('mysql-migrations');

var conn = mysql.createPool({connectionLimit : 10,
    host: process.env.NODE_DB_HOST,
    user: process.env.NODE_DB_USER,
    password: process.env.NODE_DB_PASSWORD,
    database: process.env.NODE_DB_NAME
});

migration.init(conn, __dirname + '/database/migrations', function() {
    console.log("Finished running migrations");
});