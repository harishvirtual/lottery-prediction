const express = require("express");
const generalHelper = require("./backend/helpers/GeneralHelper.js");
const mailer = require("./backend/config/mailer.js");
const conn = require("./backend/models/db.js");
const handlebars = require("express-handlebars");
const path = require("path");

const app = express();

app.engine('handlebars', handlebars({defaultLayout: '', extname: '.handlebars'}));
app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'handlebars');

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use((_request, response, next) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE, PUT, OPTIONS');
    next();
});


//Add User routes
require("./backend/routes/user.routes.js")(app);


// set port, listen for requests
app.listen(process.env.NODE_PORT, () => {
  console.log('Server is running on port '+process.env.NODE_PORT);
});