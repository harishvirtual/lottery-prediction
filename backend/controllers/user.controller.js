const User = require("../models/user.model.js");
const generalHelper = require("../helpers/GeneralHelper.js");
const bcrypt = require('bcryptjs')

exports.create = (request, response) => {
    if (!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const user = new User({
        name: generalHelper.mysql_real_escape_string(request.body.name),
        email: generalHelper.mysql_real_escape_string(request.body.email),
        password: bcrypt.hashSync(generalHelper.mysql_real_escape_string(request.body.password), bcrypt.genSaltSync())
        
    });

    User.create(user, (err, data) => {
        if (err)
        {
            if(err.code === 'SUSPENDED')
            response.status(401).send({
                error: err
            });
        }
       
        else
        {
            response.send(data);
        } 
    });
};

exports.login = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            message: "Invalid request format!"
        });
        return;
    }

    const email = generalHelper.mysql_real_escape_string(request.body.email);
    const password =generalHelper.mysql_real_escape_string(request.body.password);

    User.login(email,password, (err, data) => {
        if (err)
        {
            if(err.code === 'SERVER-ERROR')
            response.status(401).send({
            error: err
            });

            else if(err.code === 'SUSPENDED')
            response.status(402).send({
            error: err
            })
            else if(err.code === 'INVALID')
            response.status(403).send({
            error: err
            })
            
        }
        
        else
        {
            response.status(200).send(data);
        } 
    })
}