const conn = require("./db.js");
const generalHelper = require("../helpers/GeneralHelper.js");
const passwordHasher = require("../config/hasher.config.js");

const User = function(user) {
    this.name = user.name;
    this.email = user.email;
    this.password = user.password;
}

User.create = (newUser, result) => {
    conn.query(`SELECT * FROM users WHERE email = '${newUser.email}'`, (err,res) => {
        
        if(err) {
            result({"code": "SERVER-ERROR", "description": err}, null);
            return;
        }
        else {
            if(res.length)
            {
                
                result({"code": "SUSPENDED", "description": "User Already exists"}, null);
               
            }
            else 
            {
                
                conn.query("INSERT INTO users SET ?", newUser, (err, res) => {
                    if (err) {
                        result(err, null);
                        return;
                    }
                    result(null, { id: res.insertId, ...newUser });
                });
            }
        }
            
       

        
    
    });
    
    
}

User.login = (email, password, result) => {

    conn.query(`SELECT * FROM users WHERE email = '${email}'`, (err, res) => {
        if(err) {
            result({"code": "SERVER-ERROR", "description": err}, null);
            return;
        }
        if(!generalHelper.isEmptyObject(res)) {

            passwordHasher.comparePassword(password, res[0].password, (err, isPasswordMatch) => {
                if(err) {
                    result(err, null);
                }
                else {
                    if(isPasswordMatch) {
                        if(res[0].status === 1) {
                          
                            result(null, res[0]);
                           
                        }
                        else {
                            result({"code": "SUSPENDED", "description": "Your account has been suspended!"}, null);
                        }
                    }
                    else {
                        result({"code": "INVALID", "description": "Invalid Credentials!"}, null);
                    }
                }
            });
        }
        else {
            result({"code": "INVALID", "description": "Invalid Credentials!"}, null);
        }
    });
}

module.exports = User;