require('dotenv').config();
const nodemailer = require("nodemailer");
const handleBars = require("nodemailer-express-handlebars");

module.exports = function(receiverEmail, receiverName, messageSubject, messageBody, templateName, verification_link="/") {
    
    this.receiverEmail = receiverEmail;
    this.receiverName = receiverName;
    this.messageSubject = messageSubject;
    this.messageBody = messageBody;
    this.templateName = templateName;
    this.verification_link = verification_link;

    this.shootEmail = async function() {

        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.TF_MAILING_ID,
                pass: process.env.TF_MAILING_PASSWORD
            }
        });

        transporter.use('compile', handleBars({
            viewEngine:{
                partialsDir:"./templates/",
                defaultLayout:""
            },
            viewPath:"./templates/",
            extName:".handlebars"
        }));
    
        let mailInfo = await transporter.sendMail({
            from: process.env.TF_MAILING_ID,
            to: receiverEmail,
            subject: messageSubject,
            text: messageBody,
            template: templateName,
            context: {
                name: receiverName,
                verification_link: verification_link
            }
        });

        return mailInfo.messageId;
    }
}