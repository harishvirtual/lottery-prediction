export const getUser = () => {
    const userStr = localStorage.getItem('user');

    if(userStr)
        return JSON.parse(userStr);
    else
        return null;
}

export const setUserAuth = (user) => {
    localStorage.setItem('user', JSON.stringify(user));
}

export const removeUserAuth = () => {
    localStorage.removeItem('user');
    window.location.href = '/';
}