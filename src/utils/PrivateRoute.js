import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import { getUser } from './Common';

const PrivateRoute = ({component: Component, ...rest}) => {
    return (
        <Route 
        {...rest}
        render={props => {
            return getUser() ? <Component {...props} />
            :
            <Redirect to={{pathname: "/", state: {from: props.location} }} />
        }}
        />
    )
}

export default PrivateRoute;