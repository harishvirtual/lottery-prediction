import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import About from './main-website/about';
import Conatct from './main-website/contact';
import Home from './main-website/home';
import Lottery from './main-website/lottery';
import Result from './main-website/result';


function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/lottery" component={Lottery} />
          <Route path="/result" component={Result} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Conatct} />
        </Switch>
      </div>
    </Router>
    
  );
}

export default App;
