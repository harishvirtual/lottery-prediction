import React from "react";

const Footer = () => {
    return (
     
        <footer className="footer-section">
        <div className="container">
          <div className="footer-links">
            <div className="row">
              <div className="col-lg-12">
                <div className="footer-top-area">
                  <div className="left">
                    <a href="#">
                      <img src="assets/images/app_store_btn.png" alt="" />
                    </a>
                    <a href="#">
                      <img src="assets/images/goole_play_btn.png" alt="" />
                    </a>
                  </div>
                  <div className="right">
                    <ul className="links">
                      <li>
                        <a href="#">About</a>
                      </li>
                      <li>
                        <a href="#">FAQs</a>
                      </li>
                      <li>
                        <a href="#">Contact</a>
                      </li>
                      <li>
                        <a href="#">Terms of Service</a>
                      </li>
                      <li>
                        <a href="#">Privacy</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="copyright-wrapper">
            <div className="row">
              <div className="col-lg-12">
                <hr className="hr2" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 align-self-center">
                <div className="copyr-text">
                  <span>
                    Copyright © 2020.All Rights Reserved By
                  </span>
                  <a href="#">Fantra</a>
                </div>
              </div>
              <div className="col-lg-6 align-self-center">
                <ul className="footer-social-links">
                  <li>
                    <a href="#">
                      <i className="fab fa-twitter" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fab fa-facebook-f" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fab fa-instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fab fa-dribbble" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
}

export default Footer;