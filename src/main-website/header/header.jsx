import React, { useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { getUser } from '../../utils/Common';
import FavLottery from '../modal/favlottery';
import Login from '../modal/login';
import RegisterModal from '../modal/registermodal';
import AuthHeader from './authheader';
import LoginHeader from './loginheader';

const Header = () => {
  

  const [option, setOption] = useState( { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' });

    return (
        <header className="top-header">
        <div className="header-top-area">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="header-top-area-inner">
                  <Link to="/" className="logo">
                    <h2>Lottery Prediction</h2>
                  </Link>
                  {
                    getUser() ? <AuthHeader/> : <LoginHeader/> 
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="header-section">
          <div className="container">
            <div className="header-wrapper">
              <ul className="menu">
                <li>
                  <NavLink to="/" exact activeStyle>Home</NavLink>
                </li>
                <li>
                  <NavLink to="/result" activeStyle>Results</NavLink>
                </li>
                <li>
                  <NavLink to="/ai-picks" activeStyle>A.I. Picks</NavLink>
                </li>
                <li>
                  <NavLink to="/states" activeStyle>States</NavLink>
                </li>
                <li>
                  <NavLink to="/tutorial" activeStyle>Tutorial</NavLink>
                </li>
              
              </ul>
              <div className="right-tools">
                <select className="select-bar form-control" id="all_state">
                  <option value>Alabama</option>
                  <option value>Alaska</option>
                  <option value>Arizona</option>
                  <option value>Arkansas</option>
                  <option value>California</option>
                  <option value>Colorado</option>
                  <option value>Connecticut</option>
                  <option value>Delaware</option>
                  <option value>Florida</option>
                  <option value>Georgia</option>
                  <option value>Hawaii</option>
                  <option value>Idaho</option>
                  <option value>Illinois</option>
                  <option value>Indiana</option>
                  <option value>Iowa</option>
                  <option value>Kansas</option>
                  <option value>Kentucky</option>
                  <option value>Louisiana</option>
                </select>
                {/* <select className="select-bar">
                  <option value>EN</option>
                  <option value>IN</option>
                  <option value>BN</option>
                </select> */}
              </div>
              <div className="header-bar d-lg-none">
                <span>
                  <span>
                    <span>
                    </span></span></span></div>
            </div>
          </div>
        </div>
        <RegisterModal/>
        <Login/>
        <FavLottery/>
      </header>
    );
}

export default Header;