import React from "react";

const LoginHeader = ()=> {
    return (
        <div className="right-area">
                    <div className="log-reg-area">
                      <a href="#" className="custom-button1" data-toggle="modal" data-target="#registerModal">Register</a>
                      <a href="#" className="custom-button2" data-toggle="modal" data-target="#loginModal">Log
                        In</a>
                        &nbsp;&nbsp;&nbsp;
                      <a href="#" className="custom-button1" data-toggle="modal" data-target="#addLottery"><i class="fas fa-plus"></i> Add Favorite</a>
                    </div>
                     {/* <div className="cart-area">
                      <div className="icon">
                        <img src="assets/images/cart.png" alt="" />
                        <span>06</span>
                      </div>
                      <div className="amount">
                        <h4 className="mony">$ 23.60</h4>
                        <p>To checkout</p>
                      </div>
                    </div>  */}
                  </div>
        
    );
}

export default LoginHeader;