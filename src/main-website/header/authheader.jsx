import React from "react";
import { getUser, removeUserAuth } from "../../utils/Common";

const AuthHeader = ()=> {
    return (
        <div className="right-area">
                    <div className="log-reg-area">
                   
                    <a href="#!" className="custom-button2" onClick={() => removeUserAuth()}>Logout</a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="#!" className="custom-button1">{getUser().name}</a>
                         
                  </div>
                  </div>
      
    );
}
export default AuthHeader;