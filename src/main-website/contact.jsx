import React from "react";
import Footer from "./footer/footer";
import Header from "./header/header";

const Conatct = () => {
    return (
        <div>
    
        {/* ==========Overlay========== */}
        <div className="overlay" />
        <a href="#" className="scrollToTop">
          <i className="fas fa-angle-up" />
        </a>
        {/* ==========Overlay========== */}
        {/* ==========Header-Section========== */}
       <Header/>
       
        <section className="breadcrumb-area">
          <img className="contact" src="assets/images/contact-b-icon.png" alt="" />
          <div className="container">
            <div className="content">
              <h2 className="title">
                Contact
              </h2>
              <ul className="breadcrumb-list extra-padding">
                <li>
                  <a href="index.html">
                    Home
                  </a>
                </li>
                <li>
                  <a href="#">Contact</a>
                </li>
              </ul>
            </div>
          </div>
        </section>
        {/* ==========Breadcrumb-Section========== */}
        {/* ==========Contact-Section========== */}
        <section className="contact">
          <div className="container">
            <div className="row justify-content-around">
              <div className="col-lg-7">
                <div className="contact-box">
                  <h4 className="title">
                    Get In Touch
                  </h4>
                  <form action="#">
                    <div className="form-group">
                      <label>Your Name</label>
                      <input type="text" placeholder="Enter Your Name" />
                    </div>
                    <div className="form-group">
                      <label>Your Email</label>
                      <input type="email" placeholder="Enter Your Email " />
                    </div>
                    <div className="form-group last">
                      <label>Your Message</label>
                      <textarea placeholder="Enter Your Message" defaultValue={""} />
                    </div>
                    <div className="custom-control custom-checkbox">
                      <input type="checkbox" className="custom-control-input" id="customCheck1" />
                      <label className="custom-control-label" htmlFor="customCheck1">I agree to receive emails,
                        newsletters and promotional messages</label>
                    </div>
                    <button type="submit" className="custom-button1">Send Message</button>
                  </form>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="right-area">
                  <div className="faq-block">
                    <h4 className="title">Have questions?</h4>
                    <p>
                      If you have any questions or queries,
                      our helpful support team will be
                      more than happy to assist you.
                    </p>
                    <a href="#">
                      Read F.A.Q <i className="fas fa-chevron-right" />
                    </a>
                  </div>
                  <div className="contact-info">
                    <div className="single-info">
                      <img src="assets/images/eicom.png" alt="" />
                      <div className="content">
                        <h4>Email Us</h4>
                        <p><a href="https://pixner.net/cdn-cgi/l/email-protection" className="__cf_email__" data-cfemail="026b6c646d4244636c7670632c616d6f">[email&nbsp;protected]</a></p>
                      </div>
                    </div>
                    <div className="single-info">
                      <img src="assets/images/picon.png" alt="" />
                      <div className="content">
                        <h4>Call Us</h4>
                        <p>+1 (987) 435-32-11</p>
                        <p>+1 (987) 453-31-11</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Contact-Section========== */}
        {/* ==========Newslater-Section========== */}
        <Footer/>
      </div>
    );
}

export default Conatct;