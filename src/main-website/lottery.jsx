import React from "react";
import Footer from "./footer/footer";
import Header from "./header/header";

const Lottery = () => {
    return (
        <div>
      
        {/* ==========Overlay========== */}
        <div className="overlay" />
        <a href="#" className="scrollToTop">
          <i className="fas fa-angle-up" />
        </a>
        {/* ==========Overlay========== */}
        {/* ==========Header-Section========== */}
        <Header/>
     
        <section className="breadcrumb-area">
          <img className="lottory" src="assets/images/lottery-b-icon.png" alt="" />
          <div className="container">
            <div className="content">
              <h2 className="title">
                POWERBALL
              </h2>
              <ul className="breadcrumb-list extra-padding">
                <li>
                  <a href="index.html">
                    Home
                  </a>
                </li>
                <li>
                  <a href="#">Powerball</a>
                </li>
              </ul>
            </div>
          </div>
        </section>
        {/* ==========Breadcrumb-Section========== */}
        {/* ==========Singlelottery-Section========== */}
        <section className="singlelottery">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="steps">
                  <div className="left">
                    <h4>Only 3 easy steps</h4>
                  </div>
                  <div className="right">
                    <ul>
                      <li>
                        <div className="box">
                          <img src="assets/images/bt1.png" alt="" />
                          <p>1. Pick</p>
                        </div>
                      </li>
                      <li>
                        <div className="box">
                          <img src="assets/images/bt2.png" alt="" />
                          <p>2. Play</p>
                        </div>
                      </li>
                      <li>
                        <div className="box">
                          <img src="assets/images/bt3.png" alt="" />
                          <p>3. Win</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="time-wrapper">
                  <div className="time-counter">
                    <img src="assets/images/clock.png" alt="" />
                    <p className="time-countdown" data-countdown="01/01/2021" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="pick-number-area">
            <div className="container">
              <div className="row">
                <div className="col-lg-9">
                  <div className="row justify-content-center">
                    <div className="col-lg-4 col-md-6">
                      <div className="single-pick">
                        <div className="header-area">
                          <h4 className="title">Pick 5 Numbers</h4>
                          <div className="buttons">
                            <a href="#" className="custom-button1"><i className="fas fa-magic" />Quick
                              Pick</a>
                            <a href="#" className="custom-button2"><i className="fas fa-trash-alt" />Clear
                              All</a>
                          </div>
                        </div>
                        <div className="body-area">
                          <ul>
                            <li>
                              <span>1</span>
                            </li>
                            <li>
                              <span>2</span>
                            </li>
                            <li>
                              <span>3</span>
                            </li>
                            <li>
                              <span>4</span>
                            </li>
                            <li>
                              <span>5</span>
                            </li>
                            <li>
                              <span>6</span>
                            </li>
                            <li>
                              <span>7</span>
                            </li>
                            <li>
                              <span>8</span>
                            </li>
                            <li>
                              <span>9</span>
                            </li>
                            <li>
                              <span>10</span>
                            </li>
                            <li>
                              <span>11</span>
                            </li>
                            <li>
                              <span>12</span>
                            </li>
                            <li>
                              <span>13</span>
                            </li>
                            <li>
                              <span>14</span>
                            </li>
                            <li>
                              <span>15</span>
                            </li>
                            <li>
                              <span>16</span>
                            </li>
                            <li>
                              <span>17</span>
                            </li>
                            <li>
                              <span>18</span>
                            </li>
                            <li>
                              <span>19</span>
                            </li>
                            <li>
                              <span>20</span>
                            </li>
                            <li>
                              <span>21</span>
                            </li>
                            <li>
                              <span>22</span>
                            </li>
                            <li>
                              <span>23</span>
                            </li>
                            <li>
                              <span>24</span>
                            </li>
                            <li>
                              <span>25</span>
                            </li>
                            <li>
                              <span>26</span>
                            </li>
                            <li>
                              <span>27</span>
                            </li>
                            <li>
                              <span>28</span>
                            </li>
                            <li>
                              <span>29</span>
                            </li>
                            <li>
                              <span>30</span>
                            </li>
                            <li>
                              <span>31</span>
                            </li>
                            <li>
                              <span>32</span>
                            </li>
                            <li>
                              <span>33</span>
                            </li>
                            <li>
                              <span>34</span>
                            </li>
                            <li>
                              <span>35</span>
                            </li>
                            <li>
                              <span>36</span>
                            </li>
                            <li>
                              <span>37</span>
                            </li>
                            <li>
                              <span>38</span>
                            </li>
                            <li>
                              <span>39</span>
                            </li>
                            <li>
                              <span>40</span>
                            </li>
                          </ul>
                          <div className="separator">
                            <p>Pick 1 Power Ball</p>
                          </div>
                          <ul>
                            <li>
                              <span>1</span>
                            </li>
                            <li>
                              <span>2</span>
                            </li>
                            <li>
                              <span>3</span>
                            </li>
                            <li>
                              <span>4</span>
                            </li>
                            <li>
                              <span>5</span>
                            </li>
                            <li>
                              <span>6</span>
                            </li>
                            <li>
                              <span>7</span>
                            </li>
                            <li>
                              <span>8</span>
                            </li>
                            <li>
                              <span>9</span>
                            </li>
                            <li>
                              <span>10</span>
                            </li>
                            <li>
                              <span>11</span>
                            </li>
                            <li>
                              <span>12</span>
                            </li>
                            <li>
                              <span>13</span>
                            </li>
                            <li>
                              <span>14</span>
                            </li>
                            <li>
                              <span>15</span>
                            </li>
                            <li>
                              <span>16</span>
                            </li>
                            <li>
                              <span>17</span>
                            </li>
                            <li>
                              <span>18</span>
                            </li>
                            <li>
                              <span>19</span>
                            </li>
                            <li>
                              <span>20</span>
                            </li>
                            <li>
                              <span>21</span>
                            </li>
                            <li>
                              <span>22</span>
                            </li>
                            <li>
                              <span>23</span>
                            </li>
                            <li>
                              <span>24</span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-6">
                      <div className="single-pick">
                        <div className="header-area">
                          <h4 className="title">Pick 5 Numbers</h4>
                          <div className="buttons">
                            <a href="#" className="custom-button1"><i className="fas fa-magic" />Quick
                              Pick</a>
                            <a href="#" className="custom-button2"><i className="fas fa-trash-alt" />Clear
                              All</a>
                          </div>
                        </div>
                        <div className="body-area">
                          <ul>
                            <li>
                              <span>1</span>
                            </li>
                            <li>
                              <span>2</span>
                            </li>
                            <li>
                              <span>3</span>
                            </li>
                            <li>
                              <span>4</span>
                            </li>
                            <li>
                              <span>5</span>
                            </li>
                            <li>
                              <span>6</span>
                            </li>
                            <li>
                              <span>7</span>
                            </li>
                            <li>
                              <span>8</span>
                            </li>
                            <li>
                              <span>9</span>
                            </li>
                            <li>
                              <span>10</span>
                            </li>
                            <li>
                              <span>11</span>
                            </li>
                            <li>
                              <span>12</span>
                            </li>
                            <li>
                              <span>13</span>
                            </li>
                            <li>
                              <span>14</span>
                            </li>
                            <li>
                              <span>15</span>
                            </li>
                            <li>
                              <span>16</span>
                            </li>
                            <li>
                              <span>17</span>
                            </li>
                            <li>
                              <span>18</span>
                            </li>
                            <li>
                              <span>19</span>
                            </li>
                            <li>
                              <span>20</span>
                            </li>
                            <li>
                              <span>21</span>
                            </li>
                            <li>
                              <span>22</span>
                            </li>
                            <li>
                              <span>23</span>
                            </li>
                            <li>
                              <span>24</span>
                            </li>
                            <li>
                              <span>25</span>
                            </li>
                            <li>
                              <span>26</span>
                            </li>
                            <li>
                              <span>27</span>
                            </li>
                            <li>
                              <span>28</span>
                            </li>
                            <li>
                              <span>29</span>
                            </li>
                            <li>
                              <span>30</span>
                            </li>
                            <li>
                              <span>31</span>
                            </li>
                            <li>
                              <span>32</span>
                            </li>
                            <li>
                              <span>33</span>
                            </li>
                            <li>
                              <span>34</span>
                            </li>
                            <li>
                              <span>35</span>
                            </li>
                            <li>
                              <span>36</span>
                            </li>
                            <li>
                              <span>37</span>
                            </li>
                            <li>
                              <span>38</span>
                            </li>
                            <li>
                              <span>39</span>
                            </li>
                            <li>
                              <span>40</span>
                            </li>
                          </ul>
                          <div className="separator">
                            <p>Pick 1 Power Ball</p>
                          </div>
                          <ul>
                            <li>
                              <span>1</span>
                            </li>
                            <li>
                              <span>2</span>
                            </li>
                            <li>
                              <span>3</span>
                            </li>
                            <li>
                              <span>4</span>
                            </li>
                            <li>
                              <span>5</span>
                            </li>
                            <li>
                              <span>6</span>
                            </li>
                            <li>
                              <span>7</span>
                            </li>
                            <li>
                              <span>8</span>
                            </li>
                            <li>
                              <span>9</span>
                            </li>
                            <li>
                              <span>10</span>
                            </li>
                            <li>
                              <span>11</span>
                            </li>
                            <li>
                              <span>12</span>
                            </li>
                            <li>
                              <span>13</span>
                            </li>
                            <li>
                              <span>14</span>
                            </li>
                            <li>
                              <span>15</span>
                            </li>
                            <li>
                              <span>16</span>
                            </li>
                            <li>
                              <span>17</span>
                            </li>
                            <li>
                              <span>18</span>
                            </li>
                            <li>
                              <span>19</span>
                            </li>
                            <li>
                              <span>20</span>
                            </li>
                            <li>
                              <span>21</span>
                            </li>
                            <li>
                              <span>22</span>
                            </li>
                            <li>
                              <span>23</span>
                            </li>
                            <li>
                              <span>24</span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-6">
                      <div className="single-pick">
                        <div className="header-area">
                          <h4 className="title">Pick 5 Numbers</h4>
                          <div className="buttons">
                            <a href="#" className="custom-button1"><i className="fas fa-magic" />Quick
                              Pick</a>
                            <a href="#" className="custom-button2"><i className="fas fa-trash-alt" />Clear
                              All</a>
                          </div>
                        </div>
                        <div className="body-area">
                          <ul>
                            <li>
                              <span>1</span>
                            </li>
                            <li>
                              <span>2</span>
                            </li>
                            <li>
                              <span>3</span>
                            </li>
                            <li>
                              <span>4</span>
                            </li>
                            <li>
                              <span>5</span>
                            </li>
                            <li>
                              <span>6</span>
                            </li>
                            <li>
                              <span>7</span>
                            </li>
                            <li>
                              <span>8</span>
                            </li>
                            <li>
                              <span>9</span>
                            </li>
                            <li>
                              <span>10</span>
                            </li>
                            <li>
                              <span>11</span>
                            </li>
                            <li>
                              <span>12</span>
                            </li>
                            <li>
                              <span>13</span>
                            </li>
                            <li>
                              <span>14</span>
                            </li>
                            <li>
                              <span>15</span>
                            </li>
                            <li>
                              <span>16</span>
                            </li>
                            <li>
                              <span>17</span>
                            </li>
                            <li>
                              <span>18</span>
                            </li>
                            <li>
                              <span>19</span>
                            </li>
                            <li>
                              <span>20</span>
                            </li>
                            <li>
                              <span>21</span>
                            </li>
                            <li>
                              <span>22</span>
                            </li>
                            <li>
                              <span>23</span>
                            </li>
                            <li>
                              <span>24</span>
                            </li>
                            <li>
                              <span>25</span>
                            </li>
                            <li>
                              <span>26</span>
                            </li>
                            <li>
                              <span>27</span>
                            </li>
                            <li>
                              <span>28</span>
                            </li>
                            <li>
                              <span>29</span>
                            </li>
                            <li>
                              <span>30</span>
                            </li>
                            <li>
                              <span>31</span>
                            </li>
                            <li>
                              <span>32</span>
                            </li>
                            <li>
                              <span>33</span>
                            </li>
                            <li>
                              <span>34</span>
                            </li>
                            <li>
                              <span>35</span>
                            </li>
                            <li>
                              <span>36</span>
                            </li>
                            <li>
                              <span>37</span>
                            </li>
                            <li>
                              <span>38</span>
                            </li>
                            <li>
                              <span>39</span>
                            </li>
                            <li>
                              <span>40</span>
                            </li>
                          </ul>
                          <div className="separator">
                            <p>Pick 1 Power Ball</p>
                          </div>
                          <ul>
                            <li>
                              <span>1</span>
                            </li>
                            <li>
                              <span>2</span>
                            </li>
                            <li>
                              <span>3</span>
                            </li>
                            <li>
                              <span>4</span>
                            </li>
                            <li>
                              <span>5</span>
                            </li>
                            <li>
                              <span>6</span>
                            </li>
                            <li>
                              <span>7</span>
                            </li>
                            <li>
                              <span>8</span>
                            </li>
                            <li>
                              <span>9</span>
                            </li>
                            <li>
                              <span>10</span>
                            </li>
                            <li>
                              <span>11</span>
                            </li>
                            <li>
                              <span>12</span>
                            </li>
                            <li>
                              <span>13</span>
                            </li>
                            <li>
                              <span>14</span>
                            </li>
                            <li>
                              <span>15</span>
                            </li>
                            <li>
                              <span>16</span>
                            </li>
                            <li>
                              <span>17</span>
                            </li>
                            <li>
                              <span>18</span>
                            </li>
                            <li>
                              <span>19</span>
                            </li>
                            <li>
                              <span>20</span>
                            </li>
                            <li>
                              <span>21</span>
                            </li>
                            <li>
                              <span>22</span>
                            </li>
                            <li>
                              <span>23</span>
                            </li>
                            <li>
                              <span>24</span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 text-center">
                      <a href="#" className="add-ticket-btn"><i className="fas fa-plus" /> Add Tickets</a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="cart-summary">
                    <div className="top-area">
                      <h4 className="title">
                        Cart Summary
                      </h4>
                      <p className="text">
                        You've got 30% of chance to win. Shop more tickets to get more chance
                      </p>
                    </div>
                    <div className="middle-area">
                      <div className="tikit">
                        <span className="left">Filled out Tickets</span>
                        <span className="right">3</span>
                      </div>
                      <div className="price">
                        <span className="left">Ticket Price
                          <small>(8 tickets <i className="fas fa-times" /> $4.99)</small>
                        </span>
                        <span className="right">$39.92</span>
                      </div>
                    </div>
                    <div className="bottom-area">
                      <div className="total-area">
                        <span className="left">Total</span>
                        <span className="right">$39.92</span>
                      </div>
                      <a href="#" className="custom-button2">Buy Tickets</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="frequent-number">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-9">
                  <div className="section-header">
                    <h2 className="title ep">
                      Most Frequent Number
                    </h2>
                    <p className="text">
                      Check Your lotto online, find all the lotto winning numbers and see
                      if you won the latest lotto jackpots
                    </p>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <div className="number-slider owl-carousel">
                    <div className="item">
                      <div className="single-number">
                        <ul>
                          <li>
                            <span>1</span>
                          </li>
                          <li>
                            <span>2</span>
                          </li>
                          <li>
                            <span>3</span>
                          </li>
                          <li>
                            <span>4</span>
                          </li>
                          <li>
                            <span>5</span>
                          </li>
                          <li>
                            <span>6</span>
                          </li>
                        </ul>
                        <input type="text" placeholder="Try These Numbers" />
                      </div>
                    </div>
                    <div className="item">
                      <div className="single-number">
                        <ul>
                          <li>
                            <span>1</span>
                          </li>
                          <li>
                            <span>2</span>
                          </li>
                          <li>
                            <span>3</span>
                          </li>
                          <li>
                            <span>4</span>
                          </li>
                          <li>
                            <span>5</span>
                          </li>
                          <li>
                            <span>6</span>
                          </li>
                        </ul>
                        <input type="text" placeholder="Try These Numbers" />
                      </div>
                    </div>
                    <div className="item">
                      <div className="single-number">
                        <ul>
                          <li>
                            <span>1</span>
                          </li>
                          <li>
                            <span>2</span>
                          </li>
                          <li>
                            <span>3</span>
                          </li>
                          <li>
                            <span>4</span>
                          </li>
                          <li>
                            <span>5</span>
                          </li>
                          <li>
                            <span>6</span>
                          </li>
                        </ul>
                        <input type="text" placeholder="Try These Numbers" />
                      </div>
                    </div>
                    <div className="item">
                      <div className="single-number">
                        <ul>
                          <li>
                            <span>1</span>
                          </li>
                          <li>
                            <span>2</span>
                          </li>
                          <li>
                            <span>3</span>
                          </li>
                          <li>
                            <span>4</span>
                          </li>
                          <li>
                            <span>5</span>
                          </li>
                          <li>
                            <span>6</span>
                          </li>
                        </ul>
                        <input type="text" placeholder="Try These Numbers" />
                      </div>
                    </div>
                    <div className="item">
                      <div className="single-number">
                        <ul>
                          <li>
                            <span>1</span>
                          </li>
                          <li>
                            <span>2</span>
                          </li>
                          <li>
                            <span>3</span>
                          </li>
                          <li>
                            <span>4</span>
                          </li>
                          <li>
                            <span>5</span>
                          </li>
                          <li>
                            <span>6</span>
                          </li>
                        </ul>
                        <input type="text" placeholder="Try These Numbers" />
                      </div>
                    </div>
                    <div className="item">
                      <div className="single-number">
                        <ul>
                          <li>
                            <span>1</span>
                          </li>
                          <li>
                            <span>2</span>
                          </li>
                          <li>
                            <span>3</span>
                          </li>
                          <li>
                            <span>4</span>
                          </li>
                          <li>
                            <span>5</span>
                          </li>
                          <li>
                            <span>6</span>
                          </li>
                        </ul>
                        <input type="text" placeholder="Try These Numbers" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Singlelottery-Section========== */}
        {/* ==========Newslater-Section========== */}
        <Footer/>
      </div>
    );
}

export default Lottery;