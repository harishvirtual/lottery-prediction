import React from "react";
import Footer from "./footer/footer";
import Header from "./header/header";

const Result = () => {
    return (
        <div>
       
        {/* ==========Overlay========== */}
        <div className="overlay" />
        <a href="#" className="scrollToTop">
          <i className="fas fa-angle-up" />
        </a>
        {/* ==========Overlay========== */}
        {/* ==========Header-Section========== */}
        <Header/>
       
        {/* ==========Breadcrumb-Section========== */}
        <section className="breadcrumb-area">
          <div className="container">
            <div className="content">
              <h2 className="title">
                Results
              </h2>
              <ul className="breadcrumb-list extra-padding">
                <li>
                  <a href="index.html">
                    Home
                  </a>
                </li>
                <li>
                  <a href="#">Results</a>
                </li>
              </ul>
            </div>
          </div>
        </section>
        {/* ==========Breadcrumb-Section========== */}
        {/* ==========Results-Section========== */}
        <section className="results">
          <div className="top-image">
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="image">
                    <img src="assets/images/result.jpg" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="check-number result-page">
            <div className="container">
              <div className="row">
                <div className="col-lg-4 col-md-6">
                  <div className="check-box">
                    <h4 className="title">1. Select a Game</h4>
                    <div className="form-area">
                      <select>
                        <option value="#">Power Ball</option>
                        <option value="#">Megamillions</option>
                        <option value="#">Euromillions</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4 col-md-6">
                  <div className="check-box">
                    <h4 className="title">2. Pick a Date</h4>
                    <div className="form-area">
                      <input type="date" />
                    </div>
                  </div>
                </div>
                <div className="col-lg-4 col-md-6">
                  <div className="check-box">
                    <h4 className="title">3. Enter Your Number</h4>
                    <div className="form-area input-round-wrapper">
                      <input type="text" className="input-round" />
                      <input type="text" className="input-round" />
                      <input type="text" className="input-round" />
                      <input type="text" className="input-round" />
                      <input type="text" className="input-round" />
                      <input type="text" className="input-round" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="lottery-result result-page">
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="result-list">
                    <div className="single-list">
                      <div className="light-area">
                        <div className="light-area-top">
                          <div className="left">
                            <img src="assets/images/d1.png" alt="" />
                            <h4>Powerball</h4>
                          </div>
                          <div className="right">
                            <span>Next Draw</span>
                            <h6>Wed, Oct 28, 2020</h6>
                          </div>
                        </div>
                        <div className="light-area-bottom">
                          <div className="left">
                            <p>Winning Numbers:</p>
                            <div className="numbers">
                              <span>11</span>
                              <span>88</span>
                              <span>23</span>
                              <span>9</span>
                              <span>19</span>
                              <span>26</span>
                              <span>87</span>
                            </div>
                          </div>
                          <div className="right">
                            <span>Est. Jackpot</span>
                            <h6>$116 M Win BTC</h6>
                          </div>
                        </div>
                      </div>
                      <div className="color-area">
                        <div className="top">
                        <a href="#" class="custom-button1" style={{padding:"10px 86px"}}>Past Result</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 85px", marginTop:"10px"}}>Price/Odds</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 93px", marginTop:"10px"}}>Calender</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 103px", marginTop:"10px"}}>Speak</a>
                        </div>
                        {/* <div className="bottom" style={{textAlign:"center"}}>
                          <span>Est. Jackpot </span>
                          <h6>$116 M Win BTC</h6>
                        </div> */}
                      </div>
                    </div>
                    <div className="single-list">
                      <div className="light-area">
                        <div className="light-area-top">
                          <div className="left">
                            <img src="assets/images/d2.png" alt="" />
                            <h4>Megamillions</h4>
                          </div>
                          <div className="right">
                          <span>Next Draw</span>
                          <h6>Wed, Oct 28, 2020</h6>
                          </div>
                        </div>
                        <div className="light-area-bottom">
                          <div className="left">
                            <p>Winning Numbers:</p>
                            <div className="numbers">
                              <span>11</span>
                              <span>88</span>
                              <span>23</span>
                              <span>9</span>
                              <span>19</span>
                              <span>26</span>
                              <span>87</span>
                            </div>
                          </div>
                          <div className="right">
                            <span>Est. Jackpot</span>
                            <h6>$116 M Win BTC</h6>
                          </div>
                        </div>
                      </div>
                      <div className="color-area">
                        <div className="top">
                        <a href="#" class="custom-button1" style={{padding:"10px 86px"}}>Past Result</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 85px", marginTop:"10px"}}>Price/Odds</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 93px", marginTop:"10px"}}>Calender</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 103px", marginTop:"10px"}}>Speak</a>
                        </div>
                        {/* <div className="bottom" style={{textAlign:"center"}}>
                          <span>Est. Jackpot </span>
                          <h6>$116 M Win BTC</h6>
                        </div> */}
                      </div>
                    </div>
                    <div className="single-list">
                      <div className="light-area">
                        <div className="light-area-top">
                          <div className="left">
                            <img src="assets/images/d3.png" alt="" />
                            <h4>Euromillions</h4>
                          </div>
                          <div className="right">
                          <span>Next Draw</span>
                          <h6>Wed, Oct 28, 2020</h6>
                          </div>
                        </div>
                        <div className="light-area-bottom">
                          <div className="left">
                            <p>Winning Numbers:</p>
                            <div className="numbers">
                              <span>11</span>
                              <span>88</span>
                              <span>23</span>
                              <span>9</span>
                              <span>19</span>
                              <span>26</span>
                              <span>87</span>
                            </div>
                          </div>
                          <div className="right">
                            <span>Est. Jackpot</span>
                            <h6>$116 M Win BTC</h6>
                          </div>
                        </div>
                      </div>
                      <div className="color-area">
                        <div className="top">
                        <a href="#" class="custom-button1" style={{padding:"10px 86px"}}>Past Result</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 85px", marginTop:"10px"}}>Price/Odds</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 93px", marginTop:"10px"}}>Calender</a>
                        <a href="#" class="custom-button1" style={{padding:"10px 103px", marginTop:"10px"}}>Speak</a>
                        </div>
                        {/* <div className="bottom" style={{textAlign:"center"}}>
                          <span>Est. Jackpot </span>
                          <h6>$116 M Win BTC</h6>
                        </div> */}
                      </div>
                    </div>
                  </div>
                  <div className="text-center">
                    <a className="view-all" href="#">Load more </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Results-Section========== */}
        {/* ==========Newslater-Section========== */}
        <Footer/>
      </div>
    );
}

export default Result;