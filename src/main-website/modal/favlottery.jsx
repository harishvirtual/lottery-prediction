import React from "react";

const FavLottery = ()=> {
    return (
        <div className="modal fade log-reg-modal-wrapper log" id="addLottery" tabIndex={-1} aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-body">
                <div className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </div>
                <div className="log-reg-inner">
                  <h2 className="title">
                    Add Favorite Lottery
                  </h2>
                  <div className="main-content">
                    <form action="#">
                      <div className="form-group">
                        <label>State</label>
                        <select className="form-control">
                                <option value>Alabama</option>
                                <option value>Alaska</option>
                                <option value>Arizona</option>
                                <option value>Arkansas</option>
                                <option value>California</option>
                                <option value>Colorado</option>
                                <option value>Connecticut</option>
                                <option value>Delaware</option>
                                <option value>Florida</option>
                                <option value>Georgia</option>
                                <option value>Hawaii</option>
                                <option value>Idaho</option>
                                <option value>Illinois</option>
                                <option value>Indiana</option>
                                <option value>Iowa</option>
                                <option value>Kansas</option>
                                <option value>Kentucky</option>
                                <option value>Louisiana</option>
                                </select>
                      </div>
                      <div className="form-group">
                        <label>Lottery</label>
                        <select className="form-control">
                            <option>Power Ball BTC</option>
                            <option>Mega Million</option>
                            <option>Euro Millions</option>
                            <option>Lotto</option>
                        </select>
                      </div>
                      
                      <div className="button-wrapper">
                        <button type="submit" className="custom-button2">Save</button>
                      </div>
                      
                      
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
}

export default FavLottery;