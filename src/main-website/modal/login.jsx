import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import qs from "qs";
import { setUserAuth, getUser } from "../../utils/Common";
import { useHistory } from 'react-router-dom';


const Login = () => {
  const history = useHistory();

  useEffect(() => {
    if(getUser()) {
      history.replace("/result");
    }
  }, [history]);

  const [logging, setLogging] = useState(false);
  return (
    
    <div
      className="modal fade log-reg-modal-wrapper log"
      id="loginModal"
      tabIndex={-1}
      aria-hidden="true"
    >
    
      <ToastContainer/>
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-body">
            <div className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </div>
            <div className="log-reg-inner">
              <h2 className="title">Login</h2>
              <div className="main-content">
                <Formik
                  initialValues={{ email: "", password: "" }}
                  validate={(values) => {
                    const errors = {};

                    if (!values.email) {
                      errors.email = "Required";
                    } else if (
                      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(
                        values.email
                      )
                    ) {
                      errors.email = "Invalid email address";
                    }
                    if (!values.password) {
                      errors.password = "Required";
                    }
                    return errors;
                  }}
                  onSubmit={(values, { setSubmitting }) => {
                    const config = {
                      headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                      },
                    };

                    axios
                      .post(
                        process.env.REACT_APP_BASE_URL + "users/login",
                        qs.stringify(values),
                        config
                      )
                      .then((response) => {
                        console.log(response);

                        toast.success("Users successfully Verified", {
                          position: "top-right",
                          autoClose: 2000,
                          hideProgressBar: false,
                          closeOnClick: true,
                          pauseOnHover: true,
                          draggable: true,
                          progress: undefined,

                        });
                        setUserAuth(response.data);
                        setLogging(false);

                        history.replace("/result");
                      })
                      .catch((error) => {
                        toast.error(error, {
                          position: "top-right",
                          autoClose: 2000,
                          hideProgressBar: false,
                          closeOnClick: true,
                          pauseOnHover: true,
                          draggable: true,
                          progress: undefined,
                        });
                      });
                  }}
                >
                  {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                  }) => (
                    <form onSubmit={handleSubmit}>
                      <div className="form-group">
                        <label>Your Email</label>
                        <input
                          type="email"
                          className="my-form-control"
                          name="email"
                          placeholder="Enter Your Email"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.email}
                        />
                        {errors.email && touched.email && errors.email}
                      </div>
                      <div className="form-group">
                        <label>Password</label>
                        <input
                          type="password"
                          className="my-form-control"
                          placeholder="Enter Your Password"
                          name="password"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password}
                        />
                        {errors.password && touched.password && errors.password}
                      </div>
                      <p className="f-pass">
                        Forgot your password? <a href="#">recover password</a>
                      </p>
                      <div className="button-wrapper">
                        <button type="submit" className="custom-button2">
                          Sign In
                        </button>
                      </div>
                      {/* <div className="or log">
                        <p>OR</p>
                      </div>
                      <div className="or-content">
                        <p>Sign up with your email</p>
                        <a href="#" className="or-btn"><img src="assets/images/google.png" alt="" /> Sign Up with
                          Google</a>
                        <p className="or-signup">
                          Don't have an account? <a href="#" data-toggle="modal" data-target="#registerModal">
                            Sign up here
                          </a>
                        </p>
                      </div> */}
                    </form>
                  )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
