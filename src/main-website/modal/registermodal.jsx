import { Formik } from "formik";
import React from "react";
import axios from 'axios';

import qs from 'qs';
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const RegisterModal = () => {
  return (
    <div
      className="modal fade log-reg-modal-wrapper"
      id="registerModal"
      tabIndex={-1}
      aria-hidden="true"
    >
        
        <ToastContainer/>
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-body">
            <div className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </div>
            <div className="log-reg-inner">
              <h2 className="title">Create an Account</h2>
              <div className="main-content">
              <Formik
       initialValues={{ name: '', email: '', password: ''}}
       validate={values => {
         const errors = {};
         if (!values.name) {
           errors.name = 'Required';
         }
         if(!values.email) {
            errors.email = 'Required';
         }  
         else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = 'Invalid email address';
          }
         if(!values.password) {
            errors.password = 'Required';
        }
         return errors;
       }}
       onSubmit={(values, { setSubmitting }) => {
         
       
      

        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }

        axios.post(process.env.REACT_APP_BASE_URL+"users", qs.stringify(values), config)
            .then((response) => {
              //console.log(response);
              toast.success('Users created successfully', {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });

            
               
                    

                   
               
            })
            .catch((error) => {
             
              if(error.response.data.error.code === 'SUSPENDED')
              {
                
                toast.error('User Alraedy exists', {
                  position: "top-right",
                  display: "-webkit-box",
                  autoClose: 2000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
              });
              }
              else {
                toast.warning(error.response.data.error.description, {
                  position: "top-right",
                  autoClose: 2000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                })
              }
                
            });
         
       }}
     >
       {({
         values,
         errors,
         touched,
         handleChange,
         handleBlur,
         handleSubmit,
         isSubmitting,
         /* and other goodies */
       }) => (
        <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Your Name</label>
          <input
            type="text"
            className="my-form-control"
            name="name"
            placeholder="Enter Your Name"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.name}
          />
          {errors.name && touched.name && errors.name}
        </div>
        <div className="form-group">
          <label>Your Email</label>
          <input
            type="email"
            className="my-form-control"
            name="email"
            placeholder="Enter Your Email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
          />
          {errors.email && touched.email && errors.email}
        </div>
        <div className="form-group">
          <label>Password</label>
          <input
            type="password"
            className="my-form-control"
            name="password"
            placeholder="Enter Your Password"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.password}
          />
          {errors.password && touched.password && errors.password}
        </div>
     
        <div className="custom-control custom-checkbox">
          <input
            type="checkbox"
            className="custom-control-input"
            id="customCheck1"
          />
          <label
            className="custom-control-label"
            htmlFor="customCheck1"
          >
            I agree to the{" "}
            <a href="#">Terms, Privacy Policy and Fees</a>
          </label>
        </div>
        <div className="button-wrapper">
          <button type="submit" className="custom-button2">
            Register Now
          </button>
          <p className="or-signup">
            Already have an account?{" "}
            <a
              href="#"
              data-toggle="modal"
              data-target="#loginModal"
            >
              Login
            </a>
          </p>
        </div>
        {/* <div className="or">
          <p>OR</p>
        </div>
        <div className="socials">
          <a href="#">
            <i className="fab fa-facebook-f" />
          </a>
          <a href="#">
            <i className="fab fa-twitter" />
          </a>
          <a href="#">
            <i className="fab fa-google" />
          </a>
        </div> */}
      </form>
            )}
            </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegisterModal;
