import React from "react";
import Footer from "./footer/footer";
import Header from "./header/header";

const About = () => {
    return (
        <div>

        {/* ==========Overlay========== */}
        <div className="overlay" />
        <a href="#" className="scrollToTop">
          <i className="fas fa-angle-up" />
        </a>
        {/* ==========Overlay========== */}
        {/* ==========Header-Section========== */}
        <Header/>
       
        <section className="breadcrumb-area">
          <div className="container">
            <div className="content">
              <h2 className="title">
                About Us
              </h2>
              <ul className="breadcrumb-list extra-padding">
                <li>
                  <a href="index.html">
                    Home
                  </a>
                </li>
                <li>
                  <a href="#">About Us</a>
                </li>
              </ul>
            </div>
          </div>
        </section>
        {/* ==========Breadcrumb-Section========== */}
        {/* ==========About-counter-Section========== */}
        <section className="about-counter">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="about-counter-image">
                  <img src="assets/images/about-counter-bg.jpg" alt="" />
                </div>
                <div className="counter-area">
                  <div className="row justify-content-center">
                    <div className="col-lg-10">
                      <div className="counter-area-inner">
                        <div className="row">
                          <div className="col-lg-4">
                            <div className="c-box">
                              <img className="icon" src="assets/images/ac1.png" alt="" />
                              <h3 className="number">23</h3>
                              <p className="text">Winners Last Month</p>
                            </div>
                          </div>
                          <div className="col-lg-4">
                            <div className="c-box">
                              <img className="icon" src="assets/images/ac2.png" alt="" />
                              <h3 className="number">2837K</h3>
                              <p className="text">Tickets Sold</p>
                            </div>
                          </div>
                          <div className="col-lg-4">
                            <div className="c-box">
                              <img className="icon" src="assets/images/ac3.png" alt="" />
                              <h3 className="number">28387K</h3>
                              <p className="text">Payout to Winners</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========About-counter-Section========== */}
        {/* ==========About-info-Section========== */}
        <section className="about-info">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <div className="left-image">
                  <img src="assets/images/about-left.png" alt="" />
                </div>
              </div>
              <div className="col-lg-6 align-self-center">
                <div className="right-content">
                  <div className="section-header">
                    <h2 className="title">
                      About us
                    </h2>
                    <p>
                      We offer the possibility to play the world’s
                      biggest lotteries online. Our site was designed with a lottery player in mind. We are
                      lotto
                      fans ourselves, therefore we know what it takes to satisfy one.
                    </p>
                    <p>
                      Our team is build up with lottery enthusiasts, but also industry professionals. Our
                      designers and developers ensure the smoothest lotto playing experience. Support is also
                      a
                      pillar of our operations. Our agents are always thriving to help.
                    </p>
                    <p>
                      Your satisfaction is our goal!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========About-info-Section========== */}
        {/* ==========Testimonial-Section========== */}
        <section className="testimonial">
          <div className="about-feature">
            <div className="container">
              <div className="row">
                <div className="col-lg-3 col-md-6">
                  <div className="a-f-box">
                    <img src="assets/images/af1.png" alt="" />
                    <h4 className="title">100% Secure</h4>
                    <p className="text">
                      All transactions are protected by
                      GeoTrust 128-bit SSL security layer.
                    </p>
                  </div>
                </div>
                <div className="col-lg-3 col-md-6">
                  <div className="a-f-box">
                    <img src="assets/images/af2.png" alt="" />
                    <h4 className="title">No Risk</h4>
                    <p className="text">
                      All transactions are protected by
                      GeoTrust 128-bit SSL security layer.
                    </p>
                  </div>
                </div>
                <div className="col-lg-3 col-md-6">
                  <div className="a-f-box">
                    <img src="assets/images/af3.png" alt="" />
                    <h4 className="title">Support</h4>
                    <p className="text">
                      All transactions are protected by
                      GeoTrust 128-bit SSL security layer.
                    </p>
                  </div>
                </div>
                <div className="col-lg-3 col-md-6">
                  <div className="a-f-box">
                    <img src="assets/images/af4.png" alt="" />
                    <h4 className="title">Spam-Free</h4>
                    <p className="text">
                      All transactions are protected by
                      GeoTrust 128-bit SSL security layer.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="about-testimonial">
                  <img src="assets/images/map.png" alt="" />
                  <div className="client one">
                    <div className="img" data-toggle="popover-x" data-target="#myPopover" data-placement="top" data-trigger="hover focus">
                      <img src="assets/images/testi1.png" alt="" />
                    </div>
                    <div id="myPopover" className="popover popover-default mypopover">
                      <div className="arrow" />
                      <div className="client-review">
                        <p className="top-text">Awesome Fantra!</p>
                        <div className="stars">
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                        </div>
                        <p className="bottom-text">“Ideas are easy. Implementation is hard.”</p>
                        <div className="client-info">
                          <h4 className="name">Flora Oliver</h4>
                          <p className="date">Jan 1, 2021</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="client two">
                    <div className="img" data-toggle="popover-x" data-target="#myPopover2" data-placement="top" data-trigger="hover focus">
                      <img src="assets/images/testi2.png" alt="" />
                    </div>
                    <div id="myPopover2" className="popover popover-default mypopover">
                      <div className="arrow" />
                      <div className="client-review">
                        <p className="top-text">Awesome Fantra!</p>
                        <div className="stars">
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                        </div>
                        <p className="bottom-text">“Ideas are easy. Implementation is hard.”</p>
                        <div className="client-info">
                          <h4 className="name">Flora Oliver</h4>
                          <p className="date">Jan 1, 2021</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="client three">
                    <div className="img" data-toggle="popover-x" data-target="#myPopover3" data-placement="top" data-trigger="hover focus">
                      <img src="assets/images/testi3.png" alt="" />
                    </div>
                    <div id="myPopover3" className="popover popover-default mypopover">
                      <div className="arrow" />
                      <div className="client-review">
                        <p className="top-text">Awesome Fantra!</p>
                        <div className="stars">
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                        </div>
                        <p className="bottom-text">“Ideas are easy. Implementation is hard.”</p>
                        <div className="client-info">
                          <h4 className="name">Flora Oliver</h4>
                          <p className="date">Jan 1, 2021</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="client four">
                    <div className="img" data-toggle="popover-x" data-target="#myPopover4" data-placement="top" data-trigger="hover focus">
                      <img src="assets/images/testi4.png" alt="" />
                    </div>
                    <div id="myPopover4" className="popover popover-default mypopover">
                      <div className="arrow" />
                      <div className="client-review">
                        <p className="top-text">Awesome Fantra!</p>
                        <div className="stars">
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                        </div>
                        <p className="bottom-text">“Ideas are easy. Implementation is hard.”</p>
                        <div className="client-info">
                          <h4 className="name">Flora Oliver</h4>
                          <p className="date">Jan 1, 2021</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="client five">
                    <div className="img" data-toggle="popover-x" data-target="#myPopover5" data-placement="top" data-trigger="hover focus">
                      <img src="assets/images/testi5.png" alt="" />
                    </div>
                    <div id="myPopover5" className="popover popover-default mypopover">
                      <div className="arrow" />
                      <div className="client-review">
                        <p className="top-text">Awesome Fantra!</p>
                        <div className="stars">
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                        </div>
                        <p className="bottom-text">“Ideas are easy. Implementation is hard.”</p>
                        <div className="client-info">
                          <h4 className="name">Flora Oliver</h4>
                          <p className="date">Jan 1, 2021</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="client six">
                    <div className="img" data-toggle="popover-x" data-target="#myPopover6" data-placement="top" data-trigger="hover focus">
                      <img src="assets/images/testi6.png" alt="" />
                    </div>
                    <div id="myPopover6" className="popover popover-default mypopover">
                      <div className="arrow" />
                      <div className="client-review">
                        <p className="top-text">Awesome Fantra!</p>
                        <div className="stars">
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                          <i className="fas fa-star" />
                        </div>
                        <p className="bottom-text">“Ideas are easy. Implementation is hard.”</p>
                        <div className="client-info">
                          <h4 className="name">Flora Oliver</h4>
                          <p className="date">Jan 1, 2021</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Testimonial-Section========== */}
        {/* ==========Newslater-Section========== */}
        <Footer/>
      </div>
    );
}

export default About;