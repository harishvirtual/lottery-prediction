import React from "react";
import Footer from "./footer/footer";
import Header from "./header/header";
import Select from 'react-select'


const Home = () => {

 

    return (
        <div>
       
        {/* ==========Overlay========== */}
        <div className="overlay" />
        <a href="#" className="scrollToTop">
          <i className="fas fa-angle-up" />
        </a>
        {/* ==========Overlay========== */}
        {/* ==========Header-Section========== */}
        <Header/>
        
        
     
        <section className="banner-section">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <p className="banner-subtitle">Exclusive Lottery </p>
                <h1 className="banner-title">
                  Mega Jackpot
                </h1>
                <p className="text">Power up for a chance to win in this electrifying instant game!</p>
                <a href="#" className="custom-button2">Start Playing Now</a>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Banner-Section========== */}
        {/* ==========Draw-Section========== */}
        <section className="draw-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="draw-slider owl-carousel">
                  <div className="item">
                    <div className="single-draw">
                      <img className="overlay" src="assets/images/overlaymask1.png" alt="" />
                      <div className="icon">
                        <img src="assets/images/d1.png" alt="" />
                      </div>
                      <h4>
                        $116 Million
                        Win BTC
                      </h4>
                      <a href="#" className="custom-button1">Play Now</a>
                      <div className="next-draw">
                        <span className="text">Next Draw :</span>
                        <div className="time">
                          <img src="assets/images/time.png" alt="" />
                          <h6 className="time-countdown" data-countdown="01/01/2021" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="single-draw">
                      <img className="overlay" src="assets/images/overlaymask.png" alt="" />
                      <div className="icon">
                        <img src="assets/images/d2.png" alt="" />
                      </div>
                      <h4>
                        $106 Million
                        Win BTC
                      </h4>
                      <a href="#" className="custom-button2">Play Now</a>
                      <div className="next-draw">
                        <span className="text">Next Draw :</span>
                        <div className="time">
                          <img src="assets/images/time.png" alt="" />
                          <h6 className="time-countdown" data-countdown="01/06/2021" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="single-draw">
                      <img className="overlay" src="assets/images/overlaymask3.png" alt="" />
                      <div className="icon">
                        <img src="assets/images/d3.png" alt="" />
                      </div>
                      <h4>
                        $145 Million
                        Win BTC
                      </h4>
                      <a href="#" className="custom-button1">Play Now</a>
                      <div className="next-draw">
                        <span className="text">Next Draw :</span>
                        <div className="time">
                          <img src="assets/images/time.png" alt="" />
                          <h6 className="time-countdown" data-countdown="01/02/2021" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="single-draw">
                      <img className="overlay" src="assets/images/overlaymask.png" alt="" />
                      <div className="icon">
                        <img src="assets/images/d4.png" alt="" />
                      </div>
                      <h4>
                        $187 Million
                        Win BTC
                      </h4>
                      <a href="#" className="custom-button2">Play Now</a>
                      <div className="next-draw">
                        <span className="text">Next Draw :</span>
                        <div className="time">
                          <img src="assets/images/time.png" alt="" />
                          <h6 className="time-countdown" data-countdown="01/04/2021" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="single-draw">
                      <img className="overlay" src="assets/images/overlaymask3.png" alt="" />
                      <div className="icon">
                        <img src="assets/images/d3.png" alt="" />
                      </div>
                      <h4>
                        $211 Million
                        Win BTC
                      </h4>
                      <a href="#" className="custom-button1">Play Now</a>
                      <div className="next-draw">
                        <span className="text">Next Draw :</span>
                        <div className="time">
                          <img src="assets/images/time.png" alt="" />
                          <h6 className="time-countdown" data-countdown="01/05/2021" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Feature-Section========== */}
        {/* ==========Features-Section========== */}
        <section className="features-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="single-feature">
                  <div className="icon">
                    <img src="assets/images/f1.png" alt="" />
                  </div>
                  <h4 className="title">Trust</h4>
                </div>
              </div>
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="single-feature">
                  <div className="icon">
                    <img src="assets/images/f2.png" alt="" />
                  </div>
                  <h4 className="title">Safe &amp; Security</h4>
                </div>
              </div>
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="single-feature">
                  <div className="icon">
                    <img src="assets/images/f3.png" alt="" />
                  </div>
                  <h4 className="title">Zero commission</h4>
                </div>
              </div>
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="single-feature">
                  <div className="icon">
                    <img src="assets/images/f4.png" alt="" />
                  </div>
                  <h4 className="title">24/7 Support</h4>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Features-Section========== */}
        {/* ==========Lottery-Result-Section========== */}
        <section className="lottery-result">
          <img className="bg-image" src="assets/images/result-background.jpg" alt="" />
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-9">
                <div className="content">
                  <div className="section-header">
                    <h2 className="title">
                      Latest Lottery results
                    </h2>
                    <p className="text">
                      Check Your lotto online, find all the lotto winning numbers and see
                      if you won the latest lotto jackpots
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <div className="result-box">
                  {/* <h4 className="box-header">Predicting Lottery Winning Numbers</h4> */}
                  <div className="result-list">
                    <div className="single-list">
                      <div className="light-area">
                        <div className="light-area-top">
                          <div className="left">
                            <img src="assets/images/d1.png" alt="" />
                            <h4>Powerball</h4>
                          </div>
                          <div className="right">
                            <span>Draw took place on</span>
                            <h6>Saturday April 20, 2020</h6>
                          </div>
                        </div>
                        <div className="light-area-bottom">
                          <div className="left">
                            <p>Predicting Winning Numbers:</p>
                            <div className="numbers">
                              <span>11</span>
                              <span>88</span>
                              <span>23</span>
                              <span>9</span>
                              <span>19</span>
                              <span>26</span>
                              <span>87</span>
                            </div>
                          </div>
                          <div className="right">
                            <span>Est. Jackpot</span>
                            <h6>$116 M Win BTC</h6>
                          </div>
                        </div>
                      </div>
                      <div className="color-area">
                        <div className="top">
                          <span>Next Draw</span>
                          <h6>Wed, Oct 28, 2020</h6>
                        </div>
                        <div className="bottom">
                          <span>Est. Jackpot </span>
                          <h6>$116 M Win BTC</h6>
                        </div>
                      </div>
                    </div>
                    <div className="single-list">
                      <div className="light-area">
                        <div className="light-area-top">
                          <div className="left">
                            <img src="assets/images/d2.png" alt="" />
                            <h4>Megamillions</h4>
                          </div>
                          <div className="right">
                            <span>Draw took place on</span>
                            <h6>Saturday April 20, 2020</h6>
                          </div>
                        </div>
                        <div className="light-area-bottom">
                          <div className="left">
                            <p>Predicting Winning Numbers:</p>
                            <div className="numbers">
                              <span>11</span>
                              <span>88</span>
                              <span>23</span>
                              <span>9</span>
                              <span>19</span>
                              <span>26</span>
                              <span>87</span>
                            </div>
                          </div>
                          <div className="right">
                            <span>Est. Jackpot</span>
                            <h6>$116 M Win BTC</h6>
                          </div>
                        </div>
                      </div>
                      <div className="color-area">
                        <div className="top">
                          <span>Next Draw</span>
                          <h6>Wed, Oct 28, 2020</h6>
                        </div>
                        <div className="bottom">
                          <span>Est. Jackpot </span>
                          <h6>$116 M Win BTC</h6>
                        </div>
                      </div>
                    </div>
                    <div className="single-list">
                      <div className="light-area">
                        <div className="light-area-top">
                          <div className="left">
                            <img src="assets/images/d3.png" alt="" />
                            <h4>Euromillions</h4>
                          </div>
                          <div className="right">
                            <span>Draw took place on</span>
                            <h6>Saturday April 20, 2020</h6>
                          </div>
                        </div>
                        <div className="light-area-bottom">
                          <div className="left">
                            <p>Predicting Winning Numbers:</p>
                            <div className="numbers">
                              <span>11</span>
                              <span>88</span>
                              <span>23</span>
                              <span>9</span>
                              <span>19</span>
                              <span>26</span>
                              <span>87</span>
                            </div>
                          </div>
                          <div className="right">
                            <span>Est. Jackpot</span>
                            <h6>$116 M Win BTC</h6>
                          </div>
                        </div>
                      </div>
                      <div className="color-area">
                        <div className="top">
                          <span>Next Draw</span>
                          <h6>Wed, Oct 28, 2020</h6>
                        </div>
                        <div className="bottom">
                          <span>Est. Jackpot </span>
                          <h6>$116 M Win BTC</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="text-center">
                    <a className="view-all" href="#">View All Result &gt;</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Lottery-Result-Section========== */}
        {/* ==========Check-Number-Section========== */}
        <section className="check-number">
          <img className="img-left" src="assets/images/check-num-left.png" alt="" />
          <img className="img-right" src="assets/images/check-num-right.png" alt="" />
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-9">
                <div className="content">
                  <div className="section-header">
                    <h2 className="title">
                      Check your numbers
                    </h2>
                    <p className="text">
                      Are you holding on to a winning ticket? Here's an
                      easy way to find out.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4 col-md-6">
                <div className="check-box">
                  <h4 className="title">1. Select a Game</h4>
                  <div className="form-area">
                    <select>
                      <option value="#">Power Ball</option>
                      <option value="#">Megamillions</option>
                      <option value="#">Euromillions</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6">
                <div className="check-box">
                  <h4 className="title">2. Pick a Date</h4>
                  <div className="form-area">
                    <input type="date" />
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6">
                <div className="check-box">
                  <h4 className="title">3. Enter Your Number</h4>
                  <div className="form-area input-round-wrapper">
                    <input type="text" className="input-round" />
                    <input type="text" className="input-round" />
                    <input type="text" className="input-round" />
                    <input type="text" className="input-round" />
                    <input type="text" className="input-round" />
                    <input type="text" className="input-round" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ==========Check-Number-Section========== */}
        {/* ==========Newslater-Section========== */}
        <Footer/>
      </div>
    );
}

export default Home;